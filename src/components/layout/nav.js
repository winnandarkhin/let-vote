import React from 'react'
import {
    Link,
} from "react-router-dom";

const Nav = () => {
    return <div className="menu">
        <div><Link to="/">မူလစာမျက်နှာ</Link></div>
        <div><Link to="/candidate">ကိုယ်စားလှယ်လောင်း</Link></div>
        <div><Link to="/party">ပါတီရုံး</Link></div>
        <div><Link to="/voter-list">မဲဆန္ဒရှင်</Link></div>
        <div><Link to="/about">ရွေးကောက်ပွဲ အကြောင်း</Link></div>
    </div>

}
export default Nav;