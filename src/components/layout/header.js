import React from 'react'
import { Layout } from 'antd';
import './layout.css'

const { Header } = Layout;

const WebsiteHeader = () => {
    return <div className="heading">
        <div>
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS-SaZFyCesQJKdvLoK2dhQaG0SSGBkBtE5Eg&usqp=CAU" />
        </div>
        <div>
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTbLMU7OfjKN3wz4NxR3IgxKpiNAbXukyJeaQ&usqp=CAU" />
        </div>
    </div>
}
export default WebsiteHeader;