import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

import Home from '../pages/home'
import Candidate from '../pages/candidate'
import Party from '../pages/party'
import VoterList from '../pages/voter-list'
import About from '../pages/about'

const Routes = () => {
    return <Switch>
        <Route path="/" exact>
            <Home />
        </Route>
        <Route path="/candidate" exact>
            <Candidate />
        </Route>
        <Route path="/party" exact>
            <Party />
        </Route>
        <Route path="/voter-list" exact>
            <VoterList />
        </Route>
        <Route path="/about" exact>
            <About />
        </Route>
    </Switch>
}

export default Routes;