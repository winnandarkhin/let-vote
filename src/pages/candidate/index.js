import React from 'react'
import { Table, Form, Select, Row, Col } from 'antd'

const { Option } = Select;


const Candidate = () => {

    const dataSource = [
        {
            key: '1',
            name: 'Mike',
            age: 32,
            address: '10 Downing Street',
        },
        {
            key: '2',
            name: 'John',
            age: 242,
            address: '10 Downing Street',
        },
    ];

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Age',
            dataIndex: 'age',
            key: 'age',
        },
        {
            title: 'Address',
            dataIndex: 'address',
            key: 'address',
        },
    ];


    const onChange = () => {

    }


    return <div>
        <Row>
            <Col md={4} lg={4} sm={12}>
                <Form layout="vertical">
                    <Form.Item name="state_id" label="Select">
                        <Select
                            placeholder="Select a state division"
                            onChange={onChange}
                            allowClear
                        >
                            <Option value="male">male</Option>
                            <Option value="female">female</Option>
                            <Option value="other">other</Option>
                        </Select>
                    </Form.Item>

                </Form>
            </Col>
            <Col md={20} lg={20} sm={20}>
            </Col>
        </Row>
        <Row>
            <Col md={24} lg={24} sm={24}>
                <Table dataSource={dataSource} columns={columns} />;
            </Col>
        </Row>
    </div>
}

export default Candidate;